
## Các bước khởi chạy project:

#### Bước 1: Cài đặt `node_modules`
Lưu ý: Project chỉ hoạt động với NodeJs version từ 14 trở lên.

`yarn install`

#### Bước 2: Chạy lệnh sau để khởi chạy project

`yarn start`

Project sẽ được khởi chạy ở địa chỉ `localhost:3000`

Hoặc truy cập vào địa chỉ sau: [https://phannguyen-caro-game.nguyenphan.info/](https://phannguyen-caro-game.nguyenphan.info/)

Gitlab repo: [https://gitlab.com/nghieng-projects/phannguyen-caro-game](https://gitlab.com/nghieng-projects/phannguyen-caro-game).

Cảm ơn đã xem qua project của mình.




