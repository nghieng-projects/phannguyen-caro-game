import React from 'react';
import { useState } from 'react';
import styles from './CaroGame.module.scss';
const Square = ({ index, value, onClick, className }) => {
  return (
    <div className={className} onClick={() => onClick(index)}>
      {value}
    </div>
  );
};

const CaroGame = ({ squares, onCheck }) => {
  return (
    <div className={styles.game}>
      <div className={styles.squares}>
        {squares.map((value, index) => (
          <Square
            index={index}
            value={value}
            onClick={onCheck}
            className={styles.square}
          />
        ))}
      </div>
    </div>
  );
};

export default CaroGame;
