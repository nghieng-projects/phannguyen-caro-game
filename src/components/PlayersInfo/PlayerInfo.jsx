import React from 'react';

import styles from './PlayerInfo.module.scss';

const PlayerName = ({ title, value, onChange }) => {
  return (
    <div className={styles.playerName}>
      <span>{title}</span>
      <input type="text" onChange={onChange} value={value} />
    </div>
  );
};
const PlayerInfo = ({ player1, player2, onChange }) => {
  return (
    <div className={styles.playerInfo}>
      <div>Please enter players name:</div>
      <div>
        <PlayerName
          title="Player X "
          onChange={(e) => onChange(e, 1)}
          value={player1}
        />
        <PlayerName
          title="Player O "
          onChange={(e) => onChange(e, 2)}
          value={player2}
        />
      </div>
    </div>
  );
};

export default PlayerInfo;
