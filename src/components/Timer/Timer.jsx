import React from 'react';

const Timer = ({ seconds }) => {
  const minutes = Math.floor((seconds % (60 * 60)) / 60);
  const hours = Math.floor((seconds % (60 * 60 * 24)) / (60 * 60));
  const sec = Math.floor(seconds % 60);

  const time = `
  ${hours < 10 ? '0' + hours : hours}:${
    minutes < 10 ? '0' + minutes : minutes
  }:${sec < 10 ? '0' + sec : sec}`;
  return <span>{time}</span>;
};

export default Timer;
