import { useEffect, useState, useRef } from 'react';
import cln from 'classnames';
import styles from './App.module.scss';
import CaroGame from './components/CaroGame';
import PlayerInfo from './components/PlayersInfo';
import Timer from './components/Timer';

const SQUARE_NUMBER = 30;
const LIMIT_TIME = 20 * 60;// 20 minutes
const winMessage = (player, seconds) => {
  return (<div>{player} thắng!<br/>Thời gian: <Timer seconds={seconds} /></div>);
}
const App = () => {
  const [player1, setPlayer1] = useState('');
  const [player2, setPlayer2] = useState('');
  const [seconds, setSeconds] = useState(0);
  const [turn, setTurn] = useState('player1');
  const [message, setMessage] = useState();
  const [winner, setWinner] = useState();

  let timer = useRef();
  const startTimer = () => {
    timer.current = setInterval(() => {
      setSeconds((prevState) => prevState + 1);
    }, 1000);
  };

  useEffect(() => {
    if (player2 !== '' && !timer.current) {
      startTimer();
    }
  }, [player2]);

  useEffect(() => {
    if (seconds > LIMIT_TIME) {
      clearInterval(timer.current);
      setMessage('Hết giờ rồi! Kết quả: Hòa');
    }
  }, [seconds]);

  const onChangePlayer = (e, player) => {
    const { value } = e.target;
    if (player === 1) {
      setPlayer1(value);
    } else {
      setPlayer2(value);
    }
  };
  const squares1 = [];
  for (let i = 1; i <= SQUARE_NUMBER * SQUARE_NUMBER; i++) {
    squares1.push(null);
  }

  const [squares, setSquares] = useState(squares1);

  useEffect(() => {
    checkWinner();
  }, [squares]);

  const onCheck = (index) => {
    if(!player1 || !player2){
      alert('Bạn chưa nhập tên người chơi!');
      return;
    }
    if(winner) {
      return;
    }

    setSquares((prevState) => {
      if (prevState[index] === null) {
        prevState[index] = turn === 'player1' ? 'X' : 'O';
      }
      return [...prevState];
    });
    setTurn((prevState) => {
      return prevState === 'player1' ? 'player2' : 'player1';
    });
  };

  const checkWinner = () => {
    ['X', 'O'].forEach((player) => {
      const playerChecked = [];
      const selected = squares.filter((v) => v !== null);
      if (selected.length >= 9) {
        squares.forEach((value, index) => {
          if (value === player) {
            playerChecked.push(index);
          }
        });

        const playerName = player === 'X' ? player1 : player2

        checkHorizon(playerChecked, playerName);
        checkVertical(playerChecked, playerName);
        // checkCross(playerChecked, playerName)
      }
    });
  };

  const checkHorizon = (arr, player) => {
    let matchTime = 0;
    arr.sort((a, b) => a - b);
    arr.reduce((prev, current) => {
      console.log({prev, current});
      if (prev + 1 === current) {
        matchTime += 1;
        console.log(prev + 1, current, matchTime);
      } else {
        if (matchTime === 4) {
          setWinner(player);
          setMessage(winMessage(player, seconds));
          clearInterval(timer.current);
        }
        matchTime = 0;
        console.log(prev, current, matchTime);
      }
      return current;
    });

    console.log({ arr, player, matchTime });

    if (matchTime === 4) {
      setWinner(player);
      setMessage(winMessage(player, seconds));
      clearInterval(timer.current);
    }
  };

  const checkVertical = (arr, player) => {
    let matchTime = 0;
    arr.sort((a, b) => a - b);
    arr.reduce((prev, current) => {
      if (arr.includes(prev + SQUARE_NUMBER)) {
        matchTime += 1;
        return current;
      } else {
        matchTime = 0;
        return current;
      }
    });

    if (matchTime === 4) {
      setWinner(player);
      setMessage(winMessage(player, seconds));
      clearInterval(timer.current);
    }
  };

  const checkCross = (playerChecked, playerName) => {
  }

  return (
      <div className={cln(styles.container)}>
        <div className={styles.userInfo}>
          <PlayerInfo
              player1={player1}
              player2={player2}
              onChange={onChangePlayer}
          />
          <div>
            Time: <Timer seconds={seconds} />
          </div>
        </div>
        <div className={styles.message}>{message}</div>
        <CaroGame squares={squares} onCheck={onCheck} />
      </div>
  );
};

export default App;
